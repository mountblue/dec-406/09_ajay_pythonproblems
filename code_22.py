
l = []
while True:
    s = input()
    if not s:
        break
    l.append(tuple(s.split(",")))

print(sorted(l, key=lambda x: (str(x[0]),int(x[1]),int(x[2]))))
