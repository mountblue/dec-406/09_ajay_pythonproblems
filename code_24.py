import math
def robot_Move(direction, value):
    global i
    global j
    if direction == "UP":
        j = j - value
    elif direction == "DOWN":
        j = j + value
    elif direction == "LEFT":
        i = i - value
    elif direction == "RIGHT":
        i = i + value


#run the program
i = 0
j = 0
while True:
    s = input()
    if not s:
        break
    li = s.split(" ")
    robot_Move(li[0], int(li[1]))

print(math.floor(math.sqrt(i**2 + j**2)))