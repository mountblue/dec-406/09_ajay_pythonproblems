import re
#function use to check the validity of password

def isvalid(password):
    if len(password) < 6 or len(password) > 12:
        return False
    elif not re.search("[a-z]",password):
        return False
    elif not re.search("[A-Z]",password):
        return False
    elif not re.search("[0-9]",password):
        return False
    elif not re.search("[$@#]",password):
        return False
    else:
        return True


string = input("give your passwords in comma seprated password : ").split(",")
for val in string:
    if(isvalid(val)):
        print(val,",")
    
